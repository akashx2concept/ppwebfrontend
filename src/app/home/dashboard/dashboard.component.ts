import { Component, OnInit } from "@angular/core";
import { AccountManagementApiService } from "src/app/commonServices/account-management-api.service";
import { AuthHolderService } from "src/app/commonServices/auth-holder.service";



@Component({
    selector: 'PPS-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: [ './dashboard.component.scss' ]
})


export class DashboardComponent implements OnInit {


    public totalCustomer = 0;
    public totalActiveCustomer = 0;
    public totalInActiveCustomer = 0;

    public userList = [];

    constructor (
        private authHolderService: AuthHolderService,  
        private  accountManagementApiService: AccountManagementApiService,
    ) {

        
    }

    ngOnInit () {
        this.method_For_total_customer();
        this.method_For_total_Active_customer();
        this.method_For_total_IN_Active_customer();
        this.method_for_getting_User();
    }

    

    method_for_getting_User () {
        let query:any = {
          where: {
            createdBy: this.authHolderService.user['id']
          },
          order: "createdAt DESC",
          limit: 10
        }
        query = JSON.stringify(query);
        this.accountManagementApiService.getUser(query).subscribe(res => {
          this.userList = res;
          for (let q=0; q<=this.userList.length-1; q++) {
            var str = '';
            if (this.userList[q]['firstName']) str = this.userList[q]['firstName'].charAt(0);
            if (this.userList[q]['middleName']) str = str + this.userList[q]['middleName'].charAt(0);
            if (this.userList[q]['lastName']) str = str + this.userList[q]['lastName'].charAt(0);
            this.userList[q]['nameSymbol'] = str.toUpperCase();
          }
        });
      }


    method_For_total_customer () {
        let query:any = {
            createdBy: this.authHolderService.user['id']
        }
        query = JSON.stringify(query);
        this.accountManagementApiService.getUserCount(query).subscribe(res => {
            this.totalCustomer = res['count']
        })
    }

    method_For_total_Active_customer () {
        let query:any = {
            createdBy: this.authHolderService.user['id'],
            isActive: true
        }
        query = JSON.stringify(query);
        this.accountManagementApiService.getUserCount(query).subscribe(res => {
            this.totalActiveCustomer = res['count']
        })
    }

    method_For_total_IN_Active_customer () {
        let query:any = {
            createdBy: this.authHolderService.user['id'],
            isActive: false
        }
        query = JSON.stringify(query);
        this.accountManagementApiService.getUserCount(query).subscribe(res => {
            this.totalInActiveCustomer = res['count']
        })
    }


}