import { NgModule } from '@angular/core';


import { ChartJSComponent } from './chartjs.component';
import { LineChartComponent } from './line-chart/line-chart.component';
import { BarChartComponent } from './bar-chart/bar-chart.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  imports: [
    ChartsModule
  ],
  declarations: [ ChartJSComponent, LineChartComponent, BarChartComponent ],
  exports: [ChartJSComponent, LineChartComponent, BarChartComponent]
})
export class ChartJSModule { }
