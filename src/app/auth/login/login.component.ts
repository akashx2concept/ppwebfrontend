import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthApiService } from "src/app/commonServices/auth-api.service";
import { AuthHolderService } from "src/app/commonServices/auth-holder.service";

import { PPErrorStateMatcher } from "src/app/commonServices/error-state-matcher";
import { NotificationService } from "src/app/commonServices/notification.service";
import { PPPattern } from "src/app/shared/contants/pattern";

@Component({
    selector: 'PPS-login',
    templateUrl: './login.component.html',
    styleUrls: [ './login.component.scss',  ]
})


export class LoginComponent implements OnInit {

      matcher = new PPErrorStateMatcher();

    public loginForm: FormGroup;

    constructor (
        private formBuilder: FormBuilder,
        private router: Router,
        private notificationService: NotificationService,
        private authApiService: AuthApiService,
        private authHolderService: AuthHolderService
    ) {
      this.validateForm ();
    }

    ngOnInit () {

    }

    validateForm () {
        this.loginForm = this.formBuilder.group({
          email: ['', [Validators.required, Validators.email, Validators.pattern(PPPattern.email)]],
          password: ['', [Validators.required, Validators.minLength(8),
            Validators.maxLength(25),
            Validators.pattern(PPPattern.password)
          ]],
          // remenberMe:[false],
        })
    }

    get f(){
      return this.loginForm.controls;
    }

    login (){
      if(this.loginForm.invalid){
        return false;
      }
      this.authApiService.login(this.loginForm.value)
      .subscribe(res => {
        this.authHolderService.token = res;
        this.getUserDetail();
        // this.router.navigate(['/home/dashboard']);

      });
    }

    getUserDetail(){
      this.authApiService.getLoginUserDetail()
      .subscribe( (res) => {
          console.log(res);
          const me: any = res;
          if(me.role?.slug !== 'user'){
            this.authHolderService.loggedinUser = res;
            this.notificationService.get_Notification('success', 'Login Success');
            this.router.navigate(['/home/dashboard']);
          }else {
            this.notificationService.get_Notification('error', `You don't have permission to login`);
            this.authHolderService.clearToken();
          }
        })
    }
}
