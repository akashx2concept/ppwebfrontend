import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountManagmentComponent } from './account-managment.component';
import { AccountManagmentListComponent } from './account-managment-list/account-managment-list.component';
import { AccountManagmentDetailComponent } from './account-managment-detail/account-managment-detail.component';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ConfirmActionModule } from 'src/app/shared/confirm-action/confirm-action.module';
import { CreateAccountComponent } from './create-account/create-account.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UploadDocumentComponent } from './upload-document/upload-document.component';
import { DebounceModule } from 'ngx-debounce';
import { DomSanitizerInternalPipeModule } from 'src/app/customPipe/domSanitizerPipe.module';

const routes: Routes = [
  {
    path: '',
    component: AccountManagmentComponent
  }
]


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    ConfirmActionModule,
    DebounceModule,
    DomSanitizerInternalPipeModule
  ],
  declarations: [
    AccountManagmentComponent,
    AccountManagmentListComponent,
    AccountManagmentDetailComponent,
    CreateAccountComponent,
    UploadDocumentComponent
  ],
  entryComponents: [
    CreateAccountComponent,
    UploadDocumentComponent
  ]
})
export class AccountManagmentModule { }
