import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';

@Pipe({
  name: 'domSanitizerPipe'
})
export class DomSanitizerInternalPipe implements PipeTransform {

  constructor(protected sanitizer: DomSanitizer) {}
 
 public transform(value: any, type: string): SafeHtml | SafeStyle | SafeScript | SafeUrl | SafeResourceUrl {
     if (type == 'html') {
        return this.sanitizer.bypassSecurityTrustHtml(value);
     } else if (type == 'style') {
        return this.sanitizer.bypassSecurityTrustStyle(value);
     } else if (type == 'script') {
        return this.sanitizer.bypassSecurityTrustScript(value);
     } else if (type == 'url') {
        return this.sanitizer.bypassSecurityTrustUrl(value);
     } else if (type == 'resourceUrl') {
        return this.sanitizer.bypassSecurityTrustResourceUrl(value);
     }
  }
}