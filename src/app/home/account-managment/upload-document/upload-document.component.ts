 import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AccountManagementApiService } from 'src/app/commonServices/account-management-api.service';
import { AuthHolderService } from 'src/app/commonServices/auth-holder.service';
import { NotificationService } from 'src/app/commonServices/notification.service';
import { AccountManagementInternalService } from '../account-managementInternal.service';

@Component({
  selector: 'PPS-upload-document',
  templateUrl: './upload-document.component.html',
  styleUrls: ['./upload-document.component.scss']
})
export class UploadDocumentComponent implements OnInit {
  fileName;
  uploadFile = new FormGroup({
    file: new FormControl('', [Validators.required]),
    fileSource: new FormControl('', [Validators.required]),
    folderId: new FormControl('', [Validators.required])
  });


  constructor(public dialogRef: MatDialogRef<UploadDocumentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private authHolderService: AuthHolderService,
    private notificationService: NotificationService,
    private  accountManagementApiService: AccountManagementApiService,
    public accountManagementInternalService:AccountManagementInternalService
    ) { }

  ngOnInit(): void {
  }

  get f(){
    return this.uploadFile.controls;
  }

  onFileChange(event) {

    if (event.target.files.length > 0) {
      let fileObj = event.target.files[0];
      this.uploadFile.patchValue({
        fileSource: fileObj
      });
    }
  }


  submit(){
    try {
      const formData = new FormData();
      const fileObj =  this.uploadFile.get('fileSource').value;
      this.fileName = Date.now() + '.' +(fileObj.name.split('.')).pop();
      formData.append('file', fileObj, this.fileName  );

      if(this.uploadFile.invalid){
        return false;
      }
      // console.log(this.uploadFile)
      this.accountManagementApiService.uploadImageToServer(formData)
      .subscribe(res => {
        this.saveFileToServer(res);

      })
    } catch(err) {

    }

  }

  saveFileToServer(res){
    const fileObject = this.uploadFile.get('fileSource').value;
    const folderId = this.uploadFile.get('folderId').value;
    const data = {
      "url":  res.url,
      "originalName": this.fileName,
      "displayName": fileObject.name,
      "type": fileObject.type,
      "docType": fileObject.type,
      "userId": this.data.userId + '',
      "createdBy": this.authHolderService.user.id + '',
      "isFolder": false,
      "isFolderId": folderId
    }

    this.accountManagementApiService.saveImageToServer(data)
    .subscribe(res => {
      this.notificationService.get_Notification('success', 'File uploaded successfully');
      this.dialogRef.close();
    })
    //
  }

}
