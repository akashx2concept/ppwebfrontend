import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DomSanitizerInternalPipe } from './domSanitizer.pipe';





@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [    
    DomSanitizerInternalPipe
  ],
  exports: [
    DomSanitizerInternalPipe
  ]
  
})
export class DomSanitizerInternalPipeModule {
  constructor() {
    
  }
 }
