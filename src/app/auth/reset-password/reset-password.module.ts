import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { MaterialModule } from "src/app/material.module";
import { ResetPasswordComponent } from "./reset-password.component";



const routes: Routes = [
    {
        path: '',
        component: ResetPasswordComponent
    }
]



@NgModule ({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        RouterModule.forChild(routes),
        MaterialModule
    ],
    declarations: [
        ResetPasswordComponent
    ],
    exports: [
        ResetPasswordComponent
    ]
})


export class ResetPasswordModule {

}
