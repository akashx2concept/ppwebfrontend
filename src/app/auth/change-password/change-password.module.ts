import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { MaterialModule } from "src/app/material.module";
import { ChangePasswordComponent } from "./change-password.component";



const routes: Routes = [
    {
        path: '',
        component: ChangePasswordComponent
    }
]



@NgModule ({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        RouterModule.forChild(routes),
        MaterialModule
    ],
    declarations: [
        ChangePasswordComponent
    ],
    exports: [
        ChangePasswordComponent
    ]
})


export class ChangePasswordModule {

}