import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuardService } from "../commonServices/auth-guard/auth-guard.service";
import { MaterialModule } from "../material.module";
import { HomeComponent } from "./home.component";



const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: 'dashboard',
                loadChildren: () => import('./dashboard/dashboard.modeule').then(m => m.DashboardModule),
                canActivate: [AuthGuardService],
            },
            {
              path: 'account-managment',
              loadChildren: () => import('./account-managment/account-managment.module').then(m => m.AccountManagmentModule),
              canActivate: [AuthGuardService],
            }
        ]
    }
]


@NgModule({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        MaterialModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        HomeComponent
    ],
    exports: [
        HomeComponent
    ]
})

export class HomeModule {

}
