import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmActionModule } from './confirm-action/confirm-action.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ConfirmActionModule
  ],
  exports: [
    ConfirmActionModule
  ]
})
export class SharedModule { }
