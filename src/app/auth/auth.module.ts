import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuardService } from "../commonServices/auth-guard/auth-guard.service";
import { MaterialModule } from "../material.module";
import { AuthComponent } from "./auth.component";


const routes: Routes = [
    {
        path: '',
        component: AuthComponent,
        children: [
            {
                path: 'login',
                loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
                canActivate: [AuthGuardService],
            },
            {
                path: 'signup',
                loadChildren: () => import('./signup/signup.module').then(m => m.SignupModule),
                canActivate: [AuthGuardService],
            },
            {
                path: 'forgot-password',
                loadChildren: () => import('./forgot-password/forgot-password.module').then(m => m.ForgotPasswordModule),
                canActivate: [AuthGuardService],
            },
            {
                path: 'change-password',
                loadChildren: () => import('./change-password/change-password.module').then(m => m.ChangePasswordModule),
                canActivate: [AuthGuardService],
            },
            {
              path: 'reset-password',
              loadChildren: () => import('./reset-password/reset-password.module').then(m => m.ResetPasswordModule),
              canActivate: [AuthGuardService],
            },
            {
                path: 'thank-you',
                loadChildren: () => import('./thank-you/thank-you.module').then(m => m.ThankYouModule),
                canActivate: [AuthGuardService],
            }
        ]
    }
]

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule, FormsModule,
        MaterialModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        AuthComponent,
    ],
    exports: [
        AuthComponent
    ]
})


export class AuthModule {

}
