import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthApiService } from "src/app/commonServices/auth-api.service";
import { PPErrorStateMatcher } from "src/app/commonServices/error-state-matcher";
import { NotificationService } from "src/app/commonServices/notification.service";
import { PPPattern } from "src/app/shared/contants/pattern";

@Component({
    selector: 'PPS-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: [ './reset-password.component.scss' ]
})

export class ResetPasswordComponent implements OnInit {

    matcher = new PPErrorStateMatcher();
    public resetPasswordForm: FormGroup;


    constructor (
        private formBuilder: FormBuilder,
        private authApiService: AuthApiService,
        private router: Router,
        private activatedRoutes: ActivatedRoute,
        private notificationService: NotificationService
    ) {
      this.validateForm ();
    }

    ngOnInit () {
      this.activatedRoutes.queryParams.subscribe(res => {
        this.f.resetKey.setValue(res.resetKey);
      });

    }
    validateForm () {
        this.resetPasswordForm = this.formBuilder.group({
          resetKey: [],
          password: ['', [Validators.required, Validators.minLength(8),
            Validators.maxLength(25),
            Validators.pattern(PPPattern.password)
          ]],
          confirmPassword: ['', [Validators.required, Validators.minLength(8),
            Validators.maxLength(25),
            Validators.pattern(PPPattern.password)
          ]]
        }, {
          validators: this.checkPasswords
        })
    }

    checkPasswords(group: FormGroup) { // here we have the 'passwords' group
      const password = group.get('password').value;
      const confirmPassword = group.get('confirmPassword').value;

      return password === confirmPassword ? null : { notSame: true }
    }

  get f() {
    return this.resetPasswordForm.controls;
  }


  resetPassword(){
    debugger
    if(this.resetPasswordForm.invalid){
      return false;
    }

    this.authApiService.resetPassword(this.resetPasswordForm.value)
    .subscribe(res => {
      this.notificationService.get_Notification('success', 'Password has been reset successfully');
      this.router.navigate(['/auth/thank-you']);
    })

  }

}
