import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AccountManagementApiService } from 'src/app/commonServices/account-management-api.service';
import { AuthHolderService } from 'src/app/commonServices/auth-holder.service';
import { PPErrorStateMatcher } from 'src/app/commonServices/error-state-matcher';
import { NotificationService } from 'src/app/commonServices/notification.service';
import { PPPattern } from 'src/app/shared/contants/pattern';

@Component({
  selector: 'PPS-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent implements OnInit {

  matcher = new PPErrorStateMatcher();
    public createAccountForm: FormGroup;
  constructor( public dialogRef: MatDialogRef<CreateAccountComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private  accountManagementApiService: AccountManagementApiService,
    private authHolderService: AuthHolderService,
    private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.validateForm ();
  }

    validateForm () {
        this.createAccountForm = this.formBuilder.group({
          firstName: ['', [Validators.required]],
          lastName: ['', [Validators.required]],
          email: ['', [Validators.required, Validators.email, Validators.pattern(PPPattern.email)]],
          cnfPassword: ['', [
          Validators.required, Validators.minLength(8),
          Validators.maxLength(25),
          Validators.pattern(PPPattern.password)
          ]],
          password: ['',
            [Validators.required, Validators.minLength(8),
            Validators.maxLength(25),
            Validators.pattern(PPPattern.password)
          ]]
          // remenberMe:[false, [Validators.required]]
        },
        { validators: this.checkPasswords }
        )
    }

    checkPasswords(group: FormGroup) { // here we have the 'passwords' group
      const password = group.get('password').value;
      const confirmPassword = group.get('cnfPassword').value;

      return password === confirmPassword ? null : { notSame: true }
    }

    get f(){
      return this.createAccountForm.controls;
    }


    createAccount(){
      if(this.createAccountForm.invalid){
        return false;
      }
      const data = this.createAccountForm.value;
      data.createdBy = this.authHolderService.user['id'];
      delete data.cnfPassword;
      this.accountManagementApiService.createAccount(data).subscribe(res => {
          this.notificationService.get_Notification('success', 'User has been created.');
          // this.router.navigate(['/']);
          this.dialogRef.close();
      });
    }
}
