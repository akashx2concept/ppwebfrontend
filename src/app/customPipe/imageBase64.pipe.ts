import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';

@Pipe({
  name: 'imageBase64Pipe'
})

export class ImageBase64Pipe implements PipeTransform {


    constructor(private domSanitizer: DomSanitizer) {}

    transform(value: any, args?: any) {
        // let Base64;
        
        // this.getBase64Image(value).then((res) => {
        //     console.log(res)
        //     if (typeof(value) != 'undefined') {
        //         return res;
        //     } else{
        //         return '';
        //     }   
        // });

        // let d = await this.getBase64Image(value);
        // console.log(d, "++++++++++++++++++++")
        return this.getBase64Image(value);

        
    }


    // constructor(protected sanitizer: DomSanitizer) {}
 
    // public transform(imgUrl: any) {
        
    //     let data;
    //     this.getBase64Image(imgUrl).then((res) => {
    //         data = res;
    //     });
    //     return data;
    // }



    public getBase64Image(imgUrl ) {
        return new Promise((resolve, reject) => {
            var img = new Image();     
            img.onload = function(){
                var canvas = document.createElement("canvas");
                canvas.width = img.width;
                canvas.height = img.height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);
                var dataURL = canvas.toDataURL("image/png");
                //   dataURL = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
                // console.log(dataURL)
                // callback(dataURL); // the base64 string
                resolve(dataURL);
            };
            // set attributes and src 
            img.setAttribute('crossOrigin', 'anonymous'); //
            img.src = imgUrl;
        })
    }

}