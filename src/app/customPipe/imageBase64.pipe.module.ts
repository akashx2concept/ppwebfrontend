import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TruncatePipe } from './truncate.pipe';
import { ImageBase64Pipe } from './imageBase64.pipe';





@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [    
    ImageBase64Pipe
  ],
  exports: [
    ImageBase64Pipe
  ]
  
})
export class ImageBase64PipeModule {
  constructor() {
    
  }
 }
