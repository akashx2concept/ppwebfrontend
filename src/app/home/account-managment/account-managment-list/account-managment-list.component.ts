import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { rejects } from 'assert';
import { AccountManagementApiService } from 'src/app/commonServices/account-management-api.service';
import { AuthHolderService } from 'src/app/commonServices/auth-holder.service';
import { ConfirmActionComponent, DialogData } from 'src/app/shared/confirm-action/confirm-action.component';
import { AccountManagementInternalService } from '../account-managementInternal.service';
import { CreateAccountComponent } from '../create-account/create-account.component';

@Component({
  selector: 'PPS-account-managment-list',
  templateUrl: './account-managment-list.component.html',
  styleUrls: ['./account-managment-list.component.scss']
})
export class AccountManagmentListComponent implements OnInit {


  public userList = [];

  public searchUser;

  constructor(
    private dialog: MatDialog,
    private authHolderService: AuthHolderService,
    private  accountManagementApiService: AccountManagementApiService,
    public accountManagementInternalService: AccountManagementInternalService
  ) { }

  ngOnInit(): void {
    this.method_for_getting_User();
  }


  method_for_getting_User () {
    let query:any = {
      where: {
        createdBy: this.authHolderService.user['id']
      },
      order: "createdAt DESC"
    }
    if (this.searchUser) {
      query['where'] = {
            and: [{
                createdBy: this.authHolderService.user['id']
              },
              {
                or:  [
                  { firstName: { like: '%' + this.searchUser + '%' }},
                  { middleName: { like: '%' + this.searchUser + '%' }},
                  { lastName: { like: '%' + this.searchUser + '%' }},
                  { email: { like: '%' + this.searchUser + '%' }},
                ]
            }]
        };
    }

    query = encodeURI(JSON.stringify(query));
    this.accountManagementApiService.getUser(query).subscribe(res => {
      this.userList = res;
      for (let q=0; q<=this.userList.length-1; q++) {
        var str = '';
        if (this.userList[q]['firstName']) str = this.userList[q]['firstName'].charAt(0);
        if (this.userList[q]['middleName']) str = str + this.userList[q]['middleName'].charAt(0);
        if (this.userList[q]['lastName']) str = str + this.userList[q]['lastName'].charAt(0);
        this.userList[q]['nameSymbol'] = str.toUpperCase();

        this.userList[q]['isSelected'] = false;
      }
      if (this.userList.length) {
        this.userList[0]['isSelected'] = true;
        this.accountManagementInternalService.method_for_set_value(this.userList[0]);
      }
    });
  }


  method_for_chnage_data_list (item) {
    for (let q=0; q<=this.userList.length-1; q++) {
      if (this.userList[q]['id'] == item['id']) {
        this.userList[q]['isSelected'] = true;
        this.accountManagementInternalService.method_for_set_value(this.userList[q]);
      } else {
        this.userList[q]['isSelected'] = false;
      }
    }
  }

  deleteAction(item){
    const data: DialogData = {
      title: 'Deactivate Account?',
      message: 'Are you sure you want to deactivate this account?',
      yesButton: 'Deactivate',
      noButton: 'Cancel'
    }
    this.dialog.open(ConfirmActionComponent, {
      width: '40em',
      data: { ...data }
    }).afterClosed().subscribe(res => {
      if(res){
        //console.log('accepted');
        var dataDto = {
          id: item['id'],
          isActive: false
        }
        this.accountManagementApiService.updateUser(dataDto).subscribe(res => {
          this.method_for_getting_User();
        })
      }else{
        //console.log('rejected');
        this.method_for_getting_User();
      }
    })
  }

  activateUser (item) {
    const data: DialogData = {
      title: 'Activate Account?',
      message: 'Are you sure you want to activate this account?',
      yesButton: 'Activate',
      noButton: 'Cancel'
    }
    this.dialog.open(ConfirmActionComponent, {
      width: '40em',
      data: { ...data }
    }).afterClosed().subscribe(res => {
      if(res){
        //console.log('accepted');
        var dataDto = {
          id: item['id'],
          isActive: true
        }
        this.accountManagementApiService.updateUser(dataDto).subscribe(res => {
          this.method_for_getting_User();
        })
      }else{
        //console.log('rejected');
        this.method_for_getting_User();
      }
    })
  }

  createAccount(){
    this.dialog.open(CreateAccountComponent, {
      width: '40em',
      data: {}
    }).afterClosed().subscribe(res => {
      this.method_for_getting_User();
    })
  }

}
