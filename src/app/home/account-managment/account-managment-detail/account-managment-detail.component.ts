import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmActionComponent, DialogData } from 'src/app/shared/confirm-action/confirm-action.component';
import { AccountManagementInternalService } from '../account-managementInternal.service';
import { UploadDocumentComponent } from '../upload-document/upload-document.component';
import { PPApi } from '../../../commonServices/pp-api.service';

@Component({
  selector: 'PPS-account-managment-detail',
  templateUrl: './account-managment-detail.component.html',
  styleUrls: ['./account-managment-detail.component.scss']
})
export class AccountManagmentDetailComponent implements OnInit {

  public imageURL = PPApi.imageBaseUrl;

  constructor(private dialog: MatDialog,
    public accountManagementInternalService: AccountManagementInternalService
    ) { }

  ngOnInit(): void {
  }

  method_for_click_select_folder(item) {
    console.log(item)
    if (item) {
      this.accountManagementInternalService.method_For_getting_allUserImage(item.id);
    }
    
  }

  deleteAction(){
    const data: DialogData = {
      title: 'Deactivate Account',
      message: 'Would you like to deactivate account',
      yesButton: 'Deactivate',
      noButton: 'Cancel'
    }
      this.dialog.open(ConfirmActionComponent, {
        width: '400px',
        data: {
          ...data
        }
      })
      .afterClosed()
      .subscribe(res => {
        if(res){
          //console.log('accepted');
        }else{
          //console.log('rejected');
        }
      })
  }

  uploadDocumenet(){
    this.dialog.open(UploadDocumentComponent, {
      width: '40em',
      closeOnNavigation:true,
      disableClose: true,
      data: {
        userId: this.accountManagementInternalService.userData.id
      }
    }) .afterClosed()
    .subscribe(res => {
      if(res){
        this.accountManagementInternalService.method_for_set_value(this.accountManagementInternalService.userData);
        //console.log('accepted');
      }else{
        this.accountManagementInternalService.method_for_set_value(this.accountManagementInternalService.userData);
        //console.log('rejected');
      }
    })
  }
}
