import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { AuthHolderService } from "../commonServices/auth-holder.service";
import { ConfirmActionComponent } from "../shared/confirm-action/confirm-action.component";



@Component({
    selector: 'PPS-home',
    templateUrl: './home.component.html',
    styleUrls: [ './home.component.scss' ]
})


export class HomeComponent implements OnInit {



    constructor (private dialog: MatDialog, private authHolderService: AuthHolderService, private router: Router) {

    }

    ngOnInit () {

    }

    logout(){

      this.dialog.open(ConfirmActionComponent,{
        width: '30em',
        data: {
          title: 'Logout',
          message: 'Would you like to logout?'
        }
      }).afterClosed()
      .subscribe(res => {
        if(res){
          this.authHolderService.clearToken();
          this.router.navigate(['/']);

        }
      })
    }

}
