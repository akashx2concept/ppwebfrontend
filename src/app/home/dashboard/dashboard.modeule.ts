import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { MaterialModule } from "src/app/material.module";
import { ChartJSModule } from "../chartjs/chartjs.module";
import { DashboardComponent } from "./dashboard.component";


const routes: Routes = [
    {
        path: '',
        component: DashboardComponent
    }
]


@NgModule ({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        MaterialModule,
        RouterModule.forChild(routes),
        ChartJSModule
    ],
    declarations: [
        DashboardComponent
    ],
    exports: [

        DashboardComponent
    ]
})


export class DashboardModule {

}
