import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NotifierModule } from 'angular-notifier';
import { customNotifierOptions } from './shared/contants/notificationConfig';

import { PPErrorHttpInterceptor } from './shared/pp-error-httpinterceptor.service';
import { LoaderModule } from './shared/loader/loader.module';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
    LoaderModule,
    NotifierModule.withConfig(customNotifierOptions)
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: PPErrorHttpInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
