import { Injectable } from "@angular/core";
import { ImageApiService } from "src/app/commonServices/image-api.service";
import { PPApi } from '../../commonServices/pp-api.service';


@Injectable ({
    providedIn: 'root'
})

export class AccountManagementInternalService {


    public userData:any;

    public imageFolderList = [];
    public imageAllFileList = [];

    public imageURL = PPApi.imageBaseUrl;

    constructor ( private imageApiService: ImageApiService ) {

    }



    method_for_set_value (item) {
        this.userData = item;
        this.method_for_getting_image_folder(this.userData);
    }


    method_for_getting_image_folder (userData) {
        let query:any = {
            where: {
                isFolder: true,
                userId: userData['id']
            }
        }
        query = encodeURI(JSON.stringify(query));
        this.imageApiService.getImages(query).subscribe(res => {
            this.imageFolderList = res;
            this.method_For_getting_allUserImage();

            let methodArray = [];
            for (let a=0; a<=this.imageFolderList.length-1; a++) {
                let q1 = {  isFolder: false, userId: userData['id'], isFolderId: this.imageFolderList[a]['id'] }
                methodArray.push(this.method_for_getting_folderItem_count(q1));
            }

            Promise.all(methodArray).then((folderList) => {
                //console.log(folderList);
                for (let w=0; w<=this.imageFolderList.length-1; w++) {
                    this.imageFolderList[w]['count'] = 0;
                    for (let e=0; e<=folderList.length-1; e++) {
                        if (this.imageFolderList[w]['id'] == folderList[e]['query']['isFolderId']) {
                            this.imageFolderList[w]['count'] = folderList[e]['count'];
                        }
                    }
                    // if (this.imageFolderList[w]['id'] == ) {

                    // }
                }
            })
        })
    }


    method_for_getting_folderItem_count (query) {
        return new Promise((resolve, reject) => {
            let query1 = Object.assign({}, query);
            query1 = encodeURI(JSON.stringify(query1))
            this.imageApiService.getImage_count(query1).subscribe(res => {
                resolve({
                    query: query,
                    count: res['count']
                });
            })
        })
    }


    method_For_getting_allUserImage (folderId?) {
        let query:any = {
            where: {
                isFolder: false,
                userId: this.userData['id']
            }
        }
        if (folderId) {
            query['where']['isFolderId'] = folderId
        }
        query = encodeURI(JSON.stringify(query));
        this.imageApiService.getImages(query).subscribe(res => {
            this.imageAllFileList = res;
            for (let r=0; r<=this.imageAllFileList.length-1; r++) {
                this.imageAllFileList[r]['fullUrl'] = `${this.imageURL}${this.imageAllFileList[r]['url']}`;
            }
        })
    }


}
