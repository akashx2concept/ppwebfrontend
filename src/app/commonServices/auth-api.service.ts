import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import { AuthHolderService } from './auth-holder.service';
import { PPApi } from './pp-api.service';


@Injectable({
  providedIn: 'root'
})
export class AuthApiService {

  constructor(private httpClient: HttpClient){}

  signUp(data): Observable<any>{
    return this.httpClient.post<any>(PPApi.signUp, data);
  }

  login(data): Observable<any>{
    return this.httpClient.post(PPApi.login, data);
  }

  sendEmailResetPassword(data){
    return this.httpClient.post(PPApi.forgotPassword, data);
  }

  resetPassword(data){
    return this.httpClient.put(PPApi.resetPassword, data);
  }

  getLoginUserDetail(){
    return this.httpClient.get(PPApi.me);
  }

}
