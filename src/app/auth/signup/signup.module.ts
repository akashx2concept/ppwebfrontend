import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { MaterialModule } from "src/app/material.module";
import { SignupComponent } from "./signup.component";


const routes: Routes = [
    {
        path: '',
        component: SignupComponent
    }
]



@NgModule({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        RouterModule.forChild(routes),
        MaterialModule,
    ],
    declarations: [
        SignupComponent
    ],
    exports: [
        SignupComponent
    ]
})


export class SignupModule {

}