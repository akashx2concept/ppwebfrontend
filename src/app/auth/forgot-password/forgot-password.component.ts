import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthApiService } from "src/app/commonServices/auth-api.service";
import { PPErrorStateMatcher } from "src/app/commonServices/error-state-matcher";
import { NotificationService } from "src/app/commonServices/notification.service";
import { PPPattern } from "src/app/shared/contants/pattern";





@Component({
  selector: 'PPS-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {


matcher = new PPErrorStateMatcher();
public forgotPasswordForm: FormGroup;


  constructor(
    private formBuilder: FormBuilder,
    private  authApiService: AuthApiService,
    private notificationService: NotificationService,
    private router: Router
  ) {
    this.validateForm ();
  }

  ngOnInit(): void {
    // this.validateForm ();
  }
  validateForm () {
    this.forgotPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.pattern(PPPattern.email)]]
    })
  }

  get f(){
    return this.forgotPasswordForm.controls;
  }

  forgotPassword(){
    if(this.forgotPasswordForm.invalid){
      return false;
    }
    this.authApiService.sendEmailResetPassword(this.forgotPasswordForm.value)
    .subscribe(res => {
      this.notificationService.get_Notification('success', 'Please check email. Password reset link has been sent if email registered');
      this.router.navigate(['/']);
    })
  }
}
