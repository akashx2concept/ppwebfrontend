import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { MaterialModule } from "src/app/material.module";
import { ForgotPasswordComponent } from "./forgot-password.component";


const routes: Routes = [
  {
      path: '',
      component: ForgotPasswordComponent
  }
]


@NgModule({
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    RouterModule.forChild(routes),
    MaterialModule,
],
  declarations: [
      ForgotPasswordComponent
  ],
  exports: [
    ForgotPasswordComponent
  ]
})
export class ForgotPasswordModule { }
