import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { PPApi } from './pp-api.service';


@Injectable({
    providedIn: 'root'
})


export class ImageApiService {


    constructor (private httpClient: HttpClient) {

    }

    getImages(query): Observable<any> {
        return this.httpClient.get<any>(`${PPApi.images}?filter=${query}`);
    }

    getImage_count (query): Observable<any> {
        return this.httpClient.get<any>(`${PPApi.image_count}?where=${query}`);
    }


}