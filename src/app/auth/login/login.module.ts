import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { MaterialModule } from "src/app/material.module";
import { LoginComponent } from "./login.component";


const routes: Routes = [
    {
        path: '',
        component: LoginComponent
    }
]


@NgModule({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        MaterialModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        LoginComponent
    ],
    exports: [
        LoginComponent
    ]
})


export class LoginModule {

}