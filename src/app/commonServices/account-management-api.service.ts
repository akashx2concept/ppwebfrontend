import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PPApi } from './pp-api.service';


@Injectable({
  providedIn: 'root'
})
export class AccountManagementApiService {
  constructor(private httpClient: HttpClient ){}

  createAccount(data): Observable<any>{
    return this.httpClient.post<any>(PPApi.createAccount, data);
  }

  getUser (query): Observable<any> {
    return this.httpClient.get<any>(`${PPApi.users}?filter=${query}`);
  }

  getUserCount(query): Observable<any> {
    return this.httpClient.get<any>(`${PPApi.users_count}?where=${query}`);
  }

  updateUser (dataDto): Observable<any> {
    return this.httpClient.put<any>(`${PPApi.users}/${dataDto['id']}`, dataDto);
  }

  uploadImageToServer(data): Observable<any>{
    return this.httpClient.post(PPApi.uploadImage, data);
  }

  saveImageToServer(data): Observable<any>{
    return this.httpClient.post(PPApi.saveImage, data);
  }
  users_dashboardAnalysis (userId): Observable<any> {
    return this.httpClient.get<any>(`${PPApi.users_dashboardAnalysis}${userId}`);
  }


}
