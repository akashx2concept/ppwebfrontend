import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ 
    name: 'truncatepipe' 
})

export class TruncatePipe implements PipeTransform {
  transform(data: string, max) {  // replace the any with your interface for data.



    if (!data) return '';
    if (!max) return data;
    if (data.length <= max) return data;

    data = data.substr(0, max);
       
    data = data + '...';

    return data;
  }

  
}