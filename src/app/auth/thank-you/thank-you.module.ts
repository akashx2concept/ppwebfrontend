import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { MaterialModule } from "src/app/material.module";
import { ThankYouComponent } from "./thank-you.component";


const routes: Routes = [
    {
        path: '',
        component: ThankYouComponent
    }
]


@NgModule ({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        MaterialModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        ThankYouComponent
    ],
    exports: [
        ThankYouComponent
    ]
})

export class ThankYouModule {

}