import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'PPS-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit {
  // lineChart
  public lineChartData: Array<any> = [
    // {data: [65, 59, 80, 81, 56, 55, 40], label: 'Vendor'},
    // {data: [28, 48, 40, 19, 86, 27, 90], label: 'Product'},
    // {data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C'}
  ];
  public lineChartLabels: Array<any> = [];
  public lineChartOptions: any = {
    animation: false,
    responsive: true
  };
  public lineChartColours: Array<any> = [
    { // grey
      backgroundColor: 'rgb(73, 211, 204)',
      borderColor: '#3A4248',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: '#f7d470',
      borderColor: '#FCC015',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#f7d470',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';

  constructor () { }

  ngOnInit(): void {
    // Promise.all([this.dashboardInternalService.method_for_VendorProductDashboardGraph()]).then((res) => {


    // })
    this.method_for_making_graph([]);
  }

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }


  method_for_making_graph (data) {
    this.lineChartLabels = data['lineChartLabels'];
    this.lineChartData = [
      {data: data['lineVendorData'], label: 'Vendor'},
      {data: data['lineProductLabels'], label: 'Product'},
    ];
  }


}
