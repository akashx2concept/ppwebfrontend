import { Component, OnInit } from '@angular/core';
import { AccountManagementApiService } from 'src/app/commonServices/account-management-api.service';
import { AuthHolderService } from 'src/app/commonServices/auth-holder.service';


@Component({
  selector: 'PPS-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit {
// barChart
public barChartOptions: any = {
  scaleShowVerticalLines: false,
  responsive: true
};
public barChartLabels: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
public barChartType = 'bar';
public barChartLegend = true;

public barChartData: any[] = [
  {data: [65, 59, 80, 81, 56, 55, 40, 81, 56, 55, 40, 80], label: 'Active User'},
  {data: [28, 48, 40, 19, 86, 27, 90, 28, 48, 40, 70, 120], label: 'Inactive User'}
];

public barChartColours: Array<any> = [
  { // dark grey
    backgroundColor: '#64C8BC'
  },
  { // grey
    backgroundColor: '#FF8A8A'
  },
  {
    backgroundColor: "#2f353a"
  }

];
  constructor (
    private accountManagementApiService: AccountManagementApiService,
    private authHolderService: AuthHolderService,  
  ) { }

  ngOnInit(): void {

    // Promise.all([this.dashboardInternalService.method_for_orderSalesDashboardGraph()]).then((res) => {

    //   this.method_for_making_graph(res[0]);
    // })
    this.method_for_getting_graph_user();
  }

  method_for_getting_graph_user () {
    this.accountManagementApiService.users_dashboardAnalysis(this.authHolderService.user['id']).subscribe(data => {
      this.barChartLabels = data['lineChartLabels'];
      this.barChartData = [
        {data: data['lineActiveLabels'], label: 'Active User'},
        {data: data['lineInActiveData'], label: 'Inactive User'},
      ];
    })
}

  // method_for_making_graph (data) {
  //   this.barChartLabels = data['lineChartLabels'];
  //   this.barChartData = [
  //     {data: data['lineTotalOrderPlaced'], label: 'Order Placed'},
  //     {data: data['lineTotalDeliveredOrder'], label: 'Delivered Order'},
  //   ];
  // }



  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
}
