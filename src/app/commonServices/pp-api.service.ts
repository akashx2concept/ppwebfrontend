import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PPApi {

  public static readonly imageBaseUrl = environment.imageUrl;

  public static readonly login  = environment.baseUrl + 'users/refresh-login';
  public static readonly me  = environment.baseUrl + 'users/me';
  public static readonly signUp  = environment.baseUrl + 'signUp';

  public static readonly refreshToken  = environment.baseUrl + 'refresh';

  public static readonly forgotPassword  = environment.baseUrl + 'users/reset-password/init';
  public static readonly resetPassword  = environment.baseUrl + 'users/reset-password/finish';

  public static readonly createAccount  = environment.baseUrl + 'createUser';

  public static readonly users = environment.baseUrl + 'users';

  public static readonly users_dashboardAnalysis = environment.baseUrl + 'users/dashboardAnalysis/';

  public static readonly users_count = environment.baseUrl + 'users/count';


  public static readonly images = environment.baseUrl + 'images';
  public static readonly image_count = environment.baseUrl + 'images/count';

  public static readonly uploadImage = environment.baseUrl + `containers/${environment.containerName}/upload`;
  public static readonly saveImage = environment.baseUrl + `images`;
}
