import { Injectable } from "@angular/core";
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import * as _ from "lodash";
import { NotificationService } from '../notification.service';
import { AuthHolderService } from "../auth-holder.service";


@Injectable({
    providedIn: 'root'
})

export class ModuleRoutingGuardService {
    
    public isLoggedIn: boolean = false;
    public login_user_data:any;
    public login_user_token: any;
    
    public login_user_type: any;

    public current_route;


    constructor ( private router: Router, private authHolderService: AuthHolderService ) {

    }



    routing_is_authenticate_or_not (route, state, login_user_data, login_user_token) {

        // alert(1)
            this.isLoggedIn = false;
            this.login_user_data = login_user_data;
            this.login_user_token = login_user_token;
            this.current_route = route.routeConfig.path;

            // this.login_companyData = this.authHolderService.getCompanyData();

            if ( this.current_route === 'auth' ) {
                if ( this.login_user_token != null ) {
                    this.isLoggedIn = this.method_for_login_user_authenticate ( route, state, login_user_data, login_user_token );
                    return this.isLoggedIn;
                } else {
                    return true;
                }
            } else if ( this.current_route === 'login' ) {
                if ( this.login_user_token != null ) {
                    this.isLoggedIn = this.method_for_login_user_authenticate ( route, state, login_user_data, login_user_token );
                    return this.isLoggedIn;
                } else {
                    return true;
                }
            } else if ( this.current_route === 'signup' ) {
                if ( this.login_user_token != null ) {
                    this.isLoggedIn = this.method_for_login_user_authenticate ( route, state, login_user_data, login_user_token );
                    return this.isLoggedIn;
                } else {
                    return true;
                }
            } else if ( this.current_route === 'forgot-password' ) {
                if ( this.login_user_token != null ) {
                    this.isLoggedIn = this.method_for_login_user_authenticate ( route, state, login_user_data, login_user_token );
                    return this.isLoggedIn;
                } else {
                    return true;
                }
            } else if ( this.current_route === 'change-password' ) {
                if ( this.login_user_token != null ) {
                    this.isLoggedIn = this.method_for_login_user_authenticate ( route, state, login_user_data, login_user_token );
                    return this.isLoggedIn;
                } else {
                    return true;
                }
            } else if ( this.current_route === 'reset-password' ) {
                if ( this.login_user_token != null ) {
                    this.isLoggedIn = this.method_for_login_user_authenticate ( route, state, login_user_data, login_user_token );
                    return this.isLoggedIn;
                } else {
                    return true;
                }
            } else if ( this.current_route === 'thank-you' ) {
                if ( this.login_user_token != null ) {
                    this.isLoggedIn = this.method_for_login_user_authenticate ( route, state, login_user_data, login_user_token );
                    return this.isLoggedIn;
                } else {
                    return true;
                }
            } else {
                if ( this.login_user_token != null ) {
                    this.isLoggedIn = this.method_for_login_user_authenticate ( route, state, login_user_data, login_user_token );
                    return this.isLoggedIn;
                } else {
                    this.router.navigate(['auth/login'])
                }
            }
    }



    method_for_login_user_authenticate ( route, state, login_user_data, login_user_token ) {
        this.login_user_data = login_user_data;
        this.login_user_token = login_user_token;
  
        if (this.current_route == 'home') {
            this.isLoggedIn = true;
        } else if (this.current_route == 'dashboard') {
            this.isLoggedIn = true;
        } else if (this.current_route == 'account-managment') {
            this.isLoggedIn = true;
        } else {
            this.router.navigate(['home/dashboard']);
        }
        
        return this.isLoggedIn;
    }

}