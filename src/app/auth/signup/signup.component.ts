import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup,Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { AuthApiService } from "src/app/commonServices/auth-api.service";
import { PPErrorStateMatcher } from "src/app/commonServices/error-state-matcher";
import { NotificationService } from "src/app/commonServices/notification.service";
import { PPPattern } from "src/app/shared/contants/pattern";



@Component({
    selector: 'PPS-signup',
    templateUrl: './signup.component.html',
    styleUrls: [ './signup.component.scss', ]
})


export class SignupComponent implements OnInit {

    matcher = new PPErrorStateMatcher();
    public signupForm: FormGroup;

    constructor (
        private formBuilder: FormBuilder,
        private  authApiService: AuthApiService,
        private notificationService: NotificationService,
        private router: Router
    ) {
      this.validateForm ();
    }

    ngOnInit () {

    }
    validateForm () {
        this.signupForm = this.formBuilder.group({
          firstName: ['', [Validators.required]],
          email: ['', [Validators.required, Validators.email, Validators.pattern(PPPattern.email)]],
          cnfPassword: ['', [
          Validators.required, Validators.minLength(8),
          Validators.maxLength(25),
          Validators.pattern(PPPattern.password)
          ]],
          password: ['',
            [Validators.required, Validators.minLength(8),
            Validators.maxLength(25),
            Validators.pattern(PPPattern.password)
          ]]
          // remenberMe:[false, [Validators.required]]
        },
        { validators: this.checkPasswords })
    }

    checkPasswords(group: FormGroup) { // here we have the 'passwords' group
      const password = group.get('password').value;
      const confirmPassword = group.get('cnfPassword').value;

      return password === confirmPassword ? null : { notSame: true }
    }

    get f(){
      return this.signupForm.controls;
    }


    register(){
      if(this.signupForm.invalid){
        return false;
      }
      const data = this.signupForm.value;
      delete data.cnfPassword;
      this.authApiService.signUp(data).subscribe(res => {
          this.notificationService.get_Notification('success', 'User has been registerd.');
          this.router.navigate(['/']);
      });
    }
}
